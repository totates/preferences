import logging from "external:@totates/logging/v1";
import storage from 'kv-storage-polyfill';

const logger = logging.getLogger("@totates/preferences/v1");
// logger.setLevel("INFO");

const getKey = (componentName, prefName) => `${componentName}:${prefName}`;

async function del(componentName, ...prefNames)
{
    for (let prefName of prefNames) {
        const key = getKey(componentName, prefName);
        if (key in window.sessionStorage)
            delete window.sessionStorage[key];
        else
            await storage.delete(key);
    }
}

async function set(componentName, prefName, value, remember)
{
    const key = getKey(componentName, prefName);
    if (remember) {
        delete window.sessionStorage[key];
        await storage.set(key, value);
    }
    else {
        await storage.delete(key);
        window.sessionStorage[key] = JSON.stringify(value);
    }
}

async function get(componentName, prefName, defaultValue)
{
    const key = getKey(componentName, prefName);
    let val = window.sessionStorage[key];
    if (val)
        return JSON.parse(val);
    val = await storage.get(key);
    return val === undefined ? defaultValue : val;
}

function getFuncs(serviceName, remember)
{
    return {
        del: (...keys) => del(serviceName, ...keys),
        get: key => get(serviceName, key),
        set: (key, val) => set(serviceName, key, val, remember)
    };
}

export default { del, set, get, getFuncs };

/** @todo implement aioredis api
    https://aioredis.readthedocs.io/en/v1.3.0/mixins.html#generic-commands
*/
